import React, {useState,useEffect} from 'react';
import PropTypes from 'prop-types';
import {Row,Col,Card,Button} from 'react-bootstrap';


export default function index({recordsProp}) {

	const {name,type,amount,description,date} = recordsProp

	return (
		<>

			<Row className="my-3">
			  <Col>
			    <Card>
			      	<Card.Body>
			            <Card.Title>{description}</Card.Title>
			            <Card.Subtitle>{type} ({name})</Card.Subtitle>
			   			<p></p>	                
			            <Card.Subtitle>{amount}</Card.Subtitle>
			            
			            <Card.Text>{date}</Card.Text>
			      	</Card.Body>
			    </Card>
			  </Col>
			</Row>

		</>

		)
}