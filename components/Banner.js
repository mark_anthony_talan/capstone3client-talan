import {Jumbotron,Row,Col} from 'react-bootstrap';


import Link from 'next/link';



export default function Banner({dataProp}) {

	/*	Make Banner component reusable for other pages by passing data from parent to child using props
	*/
	const {title,description} = dataProp

	const date = new Date();
	const hours = date.getHours();
	let timeOfDay

	if (hours < 12) {

		timeOfDay="Morning"

		} else if (hours >= 12 && hours < 17) {

		timeOfDay = "Afternoon"

		} else {timeOfDay = "Evening"}


	return (

			<Row>
				<Col>
					<Jumbotron>

			      		<h2>Good {timeOfDay}!</h2>
			      		<p></p>
			      		<p></p>
						<h1>{title}</h1>
						<p>{description}</p>						
						
					</Jumbotron>

				</Col>
			</Row>

		)
}