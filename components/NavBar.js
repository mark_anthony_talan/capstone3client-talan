import {useContext} from 'react'
import {Navbar,Nav,Form,FormControl,Button} from 'react-bootstrap';
import Link from 'next/link';

import UserContext from '../UserContext'



export default function NavBar() {


	const {user} = useContext(UserContext)


	return (
		<>

		    
		    {
		    	user.id !== null
		    	
		    	?
			    	<Navbar bg="dark" variant="dark">
			    	<Navbar.Brand href="/">Tracker</Navbar.Brand>
			    	<Nav className="mr-auto">
				      <Nav.Link href="/categories">Categories</Nav.Link>
				      <Nav.Link href="/records">Records</Nav.Link>
				      <Nav.Link href="/monthly-expense">Monthly Expense</Nav.Link>
				      <Nav.Link href="/monthly-income">Monthly Income</Nav.Link>
				      
				    </Nav>

				    <Nav className="nav-link">
				      <Nav.Link href="/logout">Logout</Nav.Link>
				    </Nav>
				    </Navbar>
			    :
				    <Navbar bg="dark" variant="dark">
				    <Nav className="nav-link">
				      <Nav.Link href="/login">Login</Nav.Link>
				    </Nav>
				    </Navbar>

		    }

		    	
	  	

	  	</>

		)

}
