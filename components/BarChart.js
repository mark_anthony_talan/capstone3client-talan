import {Bar} from 'react-chartjs-2';
import {useState, useEffect, Fragment} from 'react';
import moment from 'moment';


export default function BarChart({rawData}) {

	console.log(rawData)

		const [months,setMonths] = useState(
			[
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December"
			]
		)

		const [dataPerMonth, setDataPerMonth] = useState()

		useEffect( () => {

			setDataPerMonth(months.map(month => {

				let data = 0
  
				rawData.forEach( element => {

					if(moment(element.date).format("MMMM")=== month){

						data += parseInt(element.amount)
						
					}				
				})

				return data
			}))

		}, [rawData])

		console.log(dataPerMonth)

		const data = {

			labels: months,
			datasets: [{

				label: 'Monthly Data',
				backgroundColor: 'green',
				borderColor: 'white',
				borderWidth: 1,
				hoverBackgroundColor: 'yellow',
				hoverBorderColor: 'black',
				data: dataPerMonth
			}]
		}

		const options = {

			scales: {
				yAxes: [
					{
						ticks: {
							beginAtZero: true
						}
					}
				]
			}
		}


		return (
			<>

				
				<Bar data={data} options={options} />		
					
			</>

			)


}
