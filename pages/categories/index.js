
import {React,useEffect,useState,useContext} from 'react';
import {Button, Table} from 'react-bootstrap';
import UserContext from '../../UserContext';
import Link from 'next/link';


export default function index() {

	const {user} = useContext(UserContext)
	const [categories, setCategories] = useState([])
	const [allCategories,setAllCategories] = useState([])
	

	useEffect( () => {
		
		let token = localStorage.getItem('token')

		fetch('https://pacific-falls-21978.herokuapp.com/api/categories', {
	        headers: {
	                "Authorization" : `Bearer ${localStorage.getItem('token')}`
	            }
        })
		
		.then( res => res.json() )
		.then( data => {
			
			setCategories(data)

			let tempArray = data.map( (array) => {
				return { name : array.name,
						type: array.type
				}
			})

			setAllCategories(tempArray)

		})

	}, []) 

	// console.log(allCategories)

	const categoryRows = allCategories.map( category => {
		return (
			<tr key={category._id}>
				<td>{category.name}</td>
				<td>{category.type}</td>
				
			</tr>
		)
	})

	return (

		<>
			<h1>Categories Page</h1>
			<Button variant="dark"  href="/categories/new">Add New Category</Button>{' '}			

				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Category</th>
							<th>Type</th>
						</tr>
					</thead>
						<tr></tr>
						<tr></tr>
					
					<tbody>
						{categoryRows}
					</tbody>
				</Table>
		</>

	)
}