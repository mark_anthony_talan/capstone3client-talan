import {useState, useEffect} from 'react';
import {Fragment} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';

export default function index() {


	const [name,setName]=useState("")
	const [type,setType]=useState("")


	function addCategory(e){

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://pacific-falls-21978.herokuapp.com/api/categories', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`				
				},
				body: JSON.stringify({
					name: name,
					type: type
				})
			})
			.then( res=> res.json() )
			.then( data => {
					console.log(data)

					if(data === true) {

		    			Swal.fire({
			                    icon: "success",
			                    title: "Successfully added the record!",
			                    text: "Thank you."
			             })

		    			Router.push('/categories')

		        		} else {
		        			Swal.fire({
			                    icon: "error",
			                    title: "Record not added",
			                    text: "Something went wrong."
		            		})
		        		}					

			})

			setName("")
			setType("")

	}


	return (
		
		<Fragment>
			<h2>Add New Category</h2>

			<Form onSubmit={ e=> addCategory(e)}>

			  	<Form.Group controlId="name">
				    <Form.Label>Category Name: </Form.Label>
				    <Form.Control 
				    	className="w-50"
				    	type="text"
				    	value={name}
				    	onChange={ (e) => setName(e.target.value)}
				    	required
				    	placeholder="Category name"
				    	/>
				</Form.Group>

				<Form.Group controlId="type">
					<Form.Label>Category Type: </Form.Label>
					<Form.Control 
						className="w-50"
						as="select"
						value={type}
						onChange={ (e) => setType(e.target.value)}
						>
						<option default hidden>Select Category Type</option>
				      <option>Income</option>
				      <option>Expense</option>
				    </Form.Control>
				</Form.Group>

				<Button variant="dark" type="submit">
			    	Submit
			  	</Button>

			</Form>

		</Fragment>

	)
}