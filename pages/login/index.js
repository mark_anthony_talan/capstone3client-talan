import Head from 'next/head'
import styles from './../../styles/Home.module.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Form, Button,Row,Col } from 'react-bootstrap';

import React,{useState,useEffect,useContext} from 'react';
import Router from 'next/router';
import Swal from 'sweetalert2'
import UserContext from './../../UserContext'
import {GoogleLogin} from 'react-google-login'

// import styles from './../../styles/mystyle.module.css'


//the landing page will be the login page
export default function Home() {

    // useContex will return an object which contains the values passed in the UserProvider
    const {user,setUser} = useContext(UserContext)
    
    // console.log(user)
    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);



    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch('https://pacific-falls-21978.herokuapp.com/api/users/login', {
            method: "POST",
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            // console.log(data);

            if(data.accessToken){

                //store token to local storage
                localStorage.setItem('token', data.accessToken)

                /*
                create a fetch request to the details route
                store the email and isAdmin values in the localStorage
                change value of user state via setUser
                */
                fetch('https://pacific-falls-21978.herokuapp.com/api/users/details', {
                    headers: {
                            "Authorization" : `Bearer ${data.accessToken}`
                        }
                })
                .then( res => res.json())
                .then( data => {

                    // console.log(data)
                    //set data from db to localstorage so 
                    localStorage.setItem('id',data._id)
                    // localStorage.setItem('isAdmin',data.isAdmin)


                    setUser({

                        id: data._id
                        // isAdmin: data.isAdmin

                    })

                    // localStorage["email"] = data.email
                    // localStorage["isAdmin"] = data.isAdmin

                    // if (localStorage["isAdmin"]==='true') {
                    //     console.log('Admin ka')
                    // } else {
                    //     console.log("D ka Admin")
                    // }
                
                })

                setEmail('');
                setPassword('');
                
                Swal.fire({
                    icon: "success",
                    title: "Successfully logged in.",
                    text: "Thank you for logging in."
                })
                //push method from router component to redirect page
                Router.push('/records')

            } else  {

                if (data.error === 'does-not-exist') {
                    Swal.fire({
                        icon: "error",
                        title: "Authentication Failed",
                        text: "User does not exist."
                    })

                } else if (data.error === 'incorrect-password') {
                    Swal.fire({
                        icon: "error",
                        title: "Authentication Failed",
                        text: "Password incorrect."
                    })
                } else if (data.error === 'login-type-error') {
                    Swal.fire({
                        icon: "error",
                        title: "Authentication Failed",
                        text: "You used a different login method."
                    })
                }
            }

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);


    function authenticateGoogleToken(response) {

        console.log(response)

        fetch('https://pacific-falls-21978.herokuapp.com/users/verify-google-id-token', {
            method: 'POST',
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({

                tokenId: response.tokenId
            })
        })
        .then(res => res.json() )
        .then(data => {

            // console.log(data);

            if(typeof data.accessToken !== 'undefined') {

                localStorage.setItem('token', data.accessToken)
                // console.log("Successful Login")
                retrieveUserDetails(data.accessToken);

            } else {

                if(data.error == 'google-auth-error') {
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed.',
                        'error'
                    )
                } else if (data.error === 'login-type-error') {
                    Swal.fire(
                        'Login Type Error',
                        'YOu may have registered thru a different login procedure',
                        'error'
                    )
                }
            }
        })

    }

    /*
        Google Login Button - react component for google login thru npm package: react-google-login

        client Id = OAuthClient id fromm Cloud Google Dev platform used for authorizing use of Google API and Google AuthClient

        onSuccess = function if successful google login
        onFailure = finction if unsuccessful

        cookiePolicy = 

        buttonText = changeable/mutable string for use of google login button text
    */

    function retrieveUserDetails(accessToken) {

        fetch('https://pacific-falls-21978.herokuapp.com/api/users/details', {
            headers: { Authorization: `Bearer ${accessToken}`}
        })
        .then(res => res.json())
        .then(data => {

            // console.log(data);
            //save to local storage for google login
            localStorage.setItem('id', data._id)
            // localStorage.setItem('isAdmin', data.isAdmin)

            setUser({
                id: data._id
                // isAdmin: data.isAdmin
            })

            Router.push('/');
        })
    }  







  return (

    <div>
    <Col md={{ span: 6, offset: 3 }}>
      <Form className="loginForm" onSubmit={(e) => authenticate(e)}>
        
        <Form.Group controlId="userEmail">
          
          <Form.Label>Email address</Form.Label>
          <Form.Control
              className="w-50" 
              type="email" 
              placeholder="Enter email" 
              value={email}
              onChange={ (e) => setEmail(e.target.value)}
              required
          />

          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>

        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            className="w-50"
            type="password"
            placeholder="Password"
            value={password}
            onChange={ (e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ?
          <Button 
            className="text-center mt-4"
            variant="dark" 
            type="submit" 
            id="submitBtn" 
            
            >
            Login
          </Button>
          :
          <Button 
            className="text-center mt-4"
            variant="danger" 
            type="submit" 
            id="submitBtn" 
            
            disabled>
            Login
          </Button>
        }

     
      </Form>

      <p></p>
      <p>Not yet registered? Sign up, <a href="/register">HERE</a></p>
         </Col>
    </div>

  )

}
