import {useContext,useEffect} from 'react';
import UserContext from '../../UserContext';

//import Router comp from nextJS to redirect user after logout
import Router from 'next/router';


export default function Logout() {

	//unwrap UserContext using useContext hook and get unsetUser funct
	const {unsetUser} = useContext(UserContext)

	//useEffect to run unsetUser funct on initial render
	useEffect( ()=> {

		unsetUser()

		Router.push('/login')

	}, [])

	//component should always return something
	return null

}