import {React,useEffect,useState,useContext} from 'react';

import Router from 'next/router';
import Link from 'next/link'
import {Row, Col, Card,InputGroup, Dropdown, DropdownButton, Button, FormControl,Form} from 'react-bootstrap';
import UserContext from './../../UserContext';

import Records from '../../components/Records'



export default function index() {


	const [name,setName]=useState("")
	const [type,setType]=useState("")

	const [allRecords,setAllRecords] = useState([])
	const [currentBalance,setCurrentBalance] = useState([])

	const [incomeArray,setIncomeArray] = useState([])
	const [expenseArray,setExpenseArray] = useState([])

	const [searchBar, setSearchBar] = useState()
	const [searchMatch, setSearchMatch] = useState([])
	
	// const [records,setRecords] = useState([])
	// const [records3,setRecords3] = useState()




	useEffect( () => {

		let token = localStorage.getItem('token')

		fetch('https://pacific-falls-21978.herokuapp.com/api/ledger', {
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then( res => res.json())
		.then( data => {
			console.log(data)

			let tempArray = data.map( array=> {
				return {
					name : array.name,
					type: array.type,
					amount: array.amount,
					description: array.description,
					date: array.createdON
				}

			})

			let currentBalance = 0 

			data.forEach( array => {

				if(array.type==='Income') {
					currentBalance += array.amount
				} else {
					currentBalance -= array.amount
				}
				return currentBalance 
				
			})


			//for income filter display
			let incomeArray = []

			data.forEach( array => {

				if(array.type==='Income') {
					incomeArray.push(array)
				}
			})

			//for expense filter display
			let expenseArray = []

			data.forEach( array => {

				if(array.type==='Expense') {
					expenseArray.push(array)
				}
			})

			setAllRecords(tempArray)
			setCurrentBalance(currentBalance)
			setIncomeArray(incomeArray)
			setExpenseArray(expenseArray)

		})




	}, []) 


		let records = []

		//display records based on filter
		if(type==='') {

			// console.log('Yes')

			records = allRecords.map(record => {

					return (
						<Records key={record._id} recordsProp={record}/>
					)
			})
		} else if(type==='Income') {
					records = incomeArray.map(record => {

							return (
								<Records key={record._id} recordsProp={record}/>
							)
					})
				} else {
						records = expenseArray.map(record => {

								return (
									<Records key={record._id} recordsProp={record}/>
								)
						})
				}

		
				

	//for the Search Bar
	useEffect( (e) => {
		
		fetch('https://pacific-falls-21978.herokuapp.com/api/ledger', {
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then( res => res.json())
		.then( data => {
			// console.log(data)

			let records2 = []
			
			data.forEach( array => {

				// console.log(array.name)
				if (array.name.includes(searchBar)) {
					
					records2.push(array)
				}
				
			})
			setSearchMatch(records2)
			// console.log(records2)
			
		})





	}, [searchBar])


		let records3 = []

		if (searchBar !=='') {

				records3 = searchMatch.map(record => {

						return (
							<Records key={record._id} recordsProp={record}/>
						)
				})		
		}

		



	return (
		<>
		
		<h1>Records Page</h1>

			<InputGroup className="mb-3">
				<InputGroup.Prepend >
			    	<Button variant="dark" href="/records/new">Add New Record</Button>
			    </InputGroup.Prepend>

			    <FormControl
			    className="mb-3"
			      placeholder="Search a Record"
			      aria-label="Search a Record"
			      aria-describedby="basic-addon2"
			      value={searchBar}
			      onChange={ (e) => setSearchBar(e.target.value)}
			    />

			  	<Form.Group controlId="type">
					<Form.Control 
						// className="mb-3"
						as="select"
						// className="w-75"
						value={type}
						onChange={ (e) => setType(e.target.value)}
						>
						<option value=''>Select Category Type</option>
					    <option value="Income">Income</option>
					    <option value="Expense">Expense</option>
				    </Form.Control>
				</Form.Group>				

			</InputGroup>



			<h4>List of all records/transactions </h4>
					<p>Your Current Balance = {currentBalance}</p>
					
					{
						searchBar !==''
						?
						<p>{records3}</p>
						:
						<p>{records}</p>
					}

		</>

		)
}