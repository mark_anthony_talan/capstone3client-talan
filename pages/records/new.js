import {useState, useEffect} from 'react';
import Router from 'next/router';
import Link from 'next/link';
import {Fragment, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from './../../UserContext'


export default function index() {

	const {user} = useContext(UserContext)

	
	// console.log(user)

	const [name,setName]=useState("")
	const [type,setType]=useState("")
	const [amount,setAmount]=useState("")
	const [description,setDescription]=useState("")
	const [income, setIncome] = useState("")
	const [expense,setExpense]= useState("")
	const [id,setID] = useState("")
	const [_id,set_ID] = useState("")


	const [newData, setNewData]= useState([])

	// const newData = data.filter( categ => categ.user === user.id)
	// console.log(newData)


	//fetch records for the specific user
	useEffect( () => {
		
		let token = localStorage.getItem('token')

		fetch('https://pacific-falls-21978.herokuapp.com/api/categories', {
	        headers: {
	                "Authorization" : `Bearer ${localStorage.getItem('token')}`
	            }
        })
		.then( res => res.json() )
		.then( data => {
			
			// console.log(data)
			setNewData(data)

		})

	}, []) 

	console.log(newData)




	// save the transaction to the DB
	function addTransaction(e) {

		console.log(newData)

		e.preventDefault()

		let token = localStorage.getItem('token')
		// let id = localStorage.getItem('id')
		console.log(name)
		fetch('https://pacific-falls-21978.herokuapp.com/api/ledger', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				"Authorization" : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				type: type,
				amount: amount,
				description: description
				// user: newData.user,
				// category: data._id

			})
		})
		.then( res => res.json() )
		.then( data => {

			console.log(data)

				if(data === true) {

		    			Swal.fire({
			                    icon: "success",
			                    title: "Successfully added the transaction!",
			                    text: "Thank you."
			             })

		    			Router.push('/records')

		        } else {
        			Swal.fire({
	                    icon: "error",
	                    title: "Transaction was not added",
	                    text: "Something went wrong."
            		})
        		}

		})

	}




	//===filter income and expense in the forms
	const incomeType = newData.filter( data => data.type === 'Income').map( data => {
	    return (
	        <option key={data._id} value={data.name}>
	            {data.name}
	        </option>
	    )
	})

	const expenseType = newData.filter( data => data.type === 'Expense').map( data => {
	    return (
	        <option key={data._id} value={data.name}>
	            {data.name}
	        </option>
	    )
	})






	return (

		<>
			<h2>Add New Record</h2>

			<Form onSubmit={ e=> addTransaction(e)}>

			  	<Form.Group controlId="type">
				    <Form.Label>Category Type: </Form.Label>
					<Form.Control 
						as="select"
						className="w-50"
						value={type}
						onChange={ (e) => setType(e.target.value)}
						>
						<option disabled value=''>Select Category Type</option>
					    <option value="Income">Income</option>
					    <option value="Expense">Expense</option>
				    </Form.Control>
				</Form.Group>

			{
				type === true
				?

				<Form.Group>
                    <Form.Label>Category Name:</Form.Label>
                    <Form.Control 
                    as="select" 
                    className="w-50"
                    value={name}
                    onChange={ (e) => setName(e.target.value)}
                    required
                    >
                        <option value='true' disabled>Select</option>
                        {incomeType}
                    </Form.Control>
                </Form.Group>

                :

					  type === "Income"
					  ? 
					  <Form.Group>
					      <Form.Label>Category Name:</Form.Label>
					      <Form.Control 
					      as="select" 
					      className="w-50"
					      value={name}
					      onChange={ (e) => setName(e.target.value)}
					      required
					      >
					          <option value='' disabled>Select</option>
					          {incomeType}
					      </Form.Control>
					  </Form.Group>
					  : 
					  	type ==="Expense"
					  	?
						  <Form.Group>
						      <Form.Label>Category Name:</Form.Label>
						      <Form.Control 
						      as="select" 
						      className="w-50"
						      value={name}
						      onChange={ (e) => setName(e.target.value)}
						      required
						      >
						          <option value='' disabled>Select</option>
						          {expenseType}
						      </Form.Control>
						  </Form.Group>
						:
						<Form.Group>
						      <Form.Label>Category Name:</Form.Label>
						      <Form.Control 
						      as="select" 
						      className="w-50"
						      value={name}
						      onChange={ (e) => setName(e.target.value)}
						      required
						      >
						          <option value='' disabled>Select</option>
						          
						      </Form.Control>
						  </Form.Group>

			}

				<Form.Group controlId="amount">
					<Form.Label>Amount: </Form.Label>
					<Form.Control
					type="number"
					className="w-50"
					value={amount}
					onChange={(e) => setAmount(e.target.value)}
					required
					/>
				</Form.Group>

				<Form.Group controlId="description">
					<Form.Label>Description: </Form.Label>
					<Form.Control
					type="text"
					className="w-50"
					value={description}
					onChange={(e) => setDescription(e.target.value)}
					required
					/>
				</Form.Group>

				<Button variant="dark" type="submit">
			    	Submit
			  	</Button>

			</Form>

		</>

		)

}


// export async function getStaticProps() {
//     const res = await fetch(`http://localhost:4000/api/ledger`)
//     const data = await res.json()
  
//     if (!data) {
//       return {
//         notFound: true,
//       }
//     }
  
//     return {
//       props: { data }, // will be passed to the page component as props
//     }
//   }