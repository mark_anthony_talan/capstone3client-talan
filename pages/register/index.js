
import React,{useState,useEffect} from 'react';
import{Form,Button,Col} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';

export default function index() {

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [mobileNo,setMobileNo] = useState(0)
	const [email,setEmail] = useState("")
	const [password1,setPassword1] = useState("")
	const [password2,setPassword2] = useState("")
	// state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	

	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e) {
		//prevents page redirection via form submission
		e.preventDefault();

		fetch('https://pacific-falls-21978.herokuapp.com/api/users/emailExist/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
        .then( res => res.json())
        .then( data => {
        	console.log(data);

        	if (data===false) {
		        	fetch('https://pacific-falls-21978.herokuapp.com/api/users/', {
		        		method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
								firstName: firstName,
								lastName: lastName,
								email: email,
								mobileNo: mobileNo,
								password: password1
								// confirmPassword: password2	
						})        		

		        	})
		        	.then( res => res.json() )
		        	.then( user => {
		        		console.log(user)

		        		if(user===true) {

		        			Swal.fire({
				                    icon: "success",
				                    title: "Successfully registered!",
				                    text: "Thank you."
				             })

		        			Router.push('/login')

		        		} else {
		        			Swal.fire({
			                    icon: "error",
			                    title: "Unsuccessful",
			                    text: "Something went wrong."
	                		})
		        		}
		        	})  

        	}	
        		else {
	        		Swal.fire({
	                    icon: "error",
	                    title: "Unsuccessful",
	                    text: "Email already used."
	                })
				}


        } )

		//clear input fields to initial values
		setFirstName('');
		setLastName('');
		setMobileNo(0);
		setEmail('');
		setPassword1('');
		setPassword2('');

	}

	useEffect(() => {

		if((firstName !=='' && lastName !=='' && mobileNo !=='' && email !== '' && password1 !== '' && password2 !=='') && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstName,lastName,mobileNo,email,password1,password2]);


	return (
		<>
		<Col md={{ span: 6, offset: 3 }}>
		<Form  className="registerForm" onSubmit={(e) => registerUser(e)}>
			<Form.Label className="text-center">Register Form</Form.Label>
			<Form.Group controlId="firstName">
				<Form.Label>First Name: </Form.Label>
				<Form.Control
				className="w-5"
				type="text"
				placeholder="Enter First Name"
				value={firstName}
				onChange={(e) => setFirstName(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name: </Form.Label>
				<Form.Control
				className="w-5"
				type="text"
				placeholder="Enter Last Name"
				value={lastName}
				onChange={(e) => setLastName(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile No: </Form.Label>
				<Form.Control
				className="w-5"
				type="number"
				value={mobileNo}
				onChange={(e) => setMobileNo(e.target.value)}
				required
				/>
				<Form.Text className="text-muted">
					Enter a valid 11-digit mobile number.	
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label>Email address</Form.Label>
				<Form.Control
				className="w-5"
				type="email"
				placeholder="Enter email"
				value={email}
				onChange={(e) => setEmail(e.target.value)}
				required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.	
				</Form.Text>
			</Form.Group>
			
			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
				className="w-5"
				type="password"
				placeholder="Password"
				value={password1}
				onChange={(e) => setPassword1(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
				className="w-5"
				type="password"
				placeholder="Verify Password"
				value={password2}
				onChange={(e) => setPassword2(e.target.value)}
				required
				/>
			</Form.Group>

			{isActive ?
				<Button className="w-5" variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button className="w-5" variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			}
		</Form>
		</Col>
		</>

		)

}

