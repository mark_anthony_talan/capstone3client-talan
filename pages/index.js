import React from 'react';
import Banner from '../components/Banner';


export default function Home() {

  let data = {
    title: "Welcome to the Income-Expense Tracker Aplication",
    description: "This simple app will help you keep track of your Income and Expenses."
    // destination: "/login",
    // label: "Go to Login Page HERE"
  }



  return (
      <React.Fragment>

         <Banner dataProp={data}/>

      </React.Fragment>

  )
}


