/* 
	This is the entry level component
*/

import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'

import React, {useState, useEffect} from 'react';
//import from component folder
import NavBar from '../components/NavBar';
import {Container} from 'react-bootstrap';
import {UserProvider} from '../UserContext';


function MyApp({ Component, pageProps }) {

	// console.log(Component)//this is your current page
	// console.log(pageProps)//contains the props to that page
	
	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	})

	//function for clearing local storage on logout
	const unsetUser = () => {
		localStorage.clear();
		setUser({
				id: null,
				isAdmin: null
		})
	}
	
	//localStorage can only be accessed after this component has been rendered
	//useEffect will only trigger on initial render
	useEffect(() => {

		setUser({
			id: localStorage.getItem('id'),
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})

	}, [])


  return (
  		
  		<div>
  			<UserProvider value={{user,setUser,unsetUser}}>
	  			<NavBar />
	  			<Container>
	  				<Component {...pageProps} />
	  			</Container>
	  		</UserProvider>
  		</div>

  	)
}

export default MyApp
