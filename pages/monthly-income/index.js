import {React,useEffect,useState,useContext} from 'react';
import UserContext from './../../UserContext';
import BarChart from '../../components/BarChart'



export default function index() {

	const [income, setIncome] = useState([]);

	useEffect( () => {

			let token = localStorage.getItem('token')

			fetch('https://pacific-falls-21978.herokuapp.com/api/ledger', {
				headers: {
					"Authorization" : `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then( res => res.json())
			.then( data => {
				// console.log(data)

				let tempArray = data.map( array => {
					return {
						amount: array.amount,
						type: array.type,
						date: array.createdON
					}
				})

				let incomeArray = tempArray.filter( array => {

						if(array.type==="Income") {
							
							incomeArray = array

							return incomeArray
						}
					})
				
				setIncome(incomeArray)
				
			})

	}, [])



	return (
		<>
			<h1>Monthly Income</h1>

			{<BarChart rawData={income}/>}

		</>
		)

}

