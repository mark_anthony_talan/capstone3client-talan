import {React,useEffect,useState,useContext} from 'react';
import UserContext from './../../UserContext';
import BarChart from '../../components/BarChart'



export default function index() {

	const [expense, setExpense] = useState([]);

	useEffect( () => {

			let token = localStorage.getItem('token')

			fetch('https://pacific-falls-21978.herokuapp.com/api/ledger', {
				headers: {
					"Authorization" : `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then( res => res.json())
			.then( data => {
				// console.log(data)

				let tempArray = data.map( array => {
					return {
						amount: array.amount,
						type: array.type,
						date: array.createdON
					}
				})

				let expenseArray = tempArray.filter( array => {

						if(array.type==="Expense") {
							
							expenseArray = array

							return expenseArray
						}
					})
				console.log(expense)
				setExpense(expenseArray)
				
			})

	}, [])



	return (
		<>
			<h1>Monthly Expense</h1>

			{<BarChart rawData={expense}/>}

		</>
		)

}