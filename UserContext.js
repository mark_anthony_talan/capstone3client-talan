import React from 'react'
/*
	userContext can share data between component 
*/

const UserContext = React.createContext() 
/* 
	Provider
	comes with Provider React component
*/

export const UserProvider = UserContext.Provider

export default UserContext